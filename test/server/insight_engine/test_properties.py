import pytest
import pprint
from server.insight_engine.Properties import *

# helper function
def my_pprint(obj):
    pp = pprint.PrettyPrinter(indent=4)
    pp.pprint(obj)

INITIAL = {
    'color': 'red',
    'number' : 3.14159,
    'treat' : 'cupcakes',
    'stooges' : ['Moe', 'Larry', 'Curly'],
    'planets' : {
        'first' : {
            'name' : 'Mercury',
            'color' : 'yellow'
        },
        'second' : {
            'name' : 'Venus',
            'color' : 'green',
        },
        'third' : {
            'name' : 'Earth',
            'color' : 'blue'
        }
    }
}

@pytest.fixture()
def props():
    return Properties(INITIAL)

def test_constructor(props):
    #my_pprint(props.getData())
    assert props != None
    assert type(props) == Properties

def test_get_simple(props):
    assert props['color'] == 'red'
    assert props['number'] == 3.14159
    assert props['stooges'] == ['Moe', 'Larry', 'Curly']

def test_get_path(props):
    assert props['planets.first.name'] == 'Mercury'
    assert props['planets.third.color'] == 'blue'

def test_class_get():
    assert Properties.get('color', INITIAL) == 'red'