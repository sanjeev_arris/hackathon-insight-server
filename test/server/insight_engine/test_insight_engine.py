import pytest
import pprint
from server.insight_engine.InsightEngine import *
from server.insight_engine.Properties import *

@pytest.fixture()
def engine():
    return InsightEngine("server/engine-defs.yml")

# helper function
def my_pprint(obj):
    pp = pprint.PrettyPrinter(indent=4)
    pp.pprint(obj)

def test_constructor(engine):
    assert engine != None
    assert type(engine) == InsightEngine

def test_get_props(engine):
    props = engine.getProperties()
    #my_pprint(props)
    assert props != None
    assert type(props) == dict


###################################################################################################

def solve_gateway_helper(engine, expected, features):
    input = {
        'features' : features,
    }
    result = engine.solve_gateway(Properties(input))
    #my_pprint({'features': features, 'result': result})
    assert result != None
    assert result['model'] == expected


def test_solve_gateway(engine):
    solve_gateway_helper(engine, "NVG443", {'WAN' : 'DSL',   'VOIP' : False, 'MoCA' : False})
    solve_gateway_helper(engine, "NVG448", {'WAN' : 'DSL',   'VOIP' : True,  'MoCA' : False})
    solve_gateway_helper(engine, "ERROR", {'WAN' : 'DSL',   'VOIP' : True,  'MoCA' : True})
    solve_gateway_helper(engine, "NVG463", {'WAN' : 'CABLE', 'VOIP' : False, 'MoCA' : False})
    solve_gateway_helper(engine, "NVG468MQ", {'WAN' : 'CABLE', 'VOIP' : True, 'MoCA' : False})
    solve_gateway_helper(engine, "NVG468MQ", {'WAN' : 'CABLE', 'VOIP' : True, 'MoCA' : True})


###################################################################################################

def mk_domicile(area, floors):
    return Properties({'domicile':{'area':area,'floors':floors}})

def test_solve_extenders_count(engine):
    assert engine.solve_extenders_count(mk_domicile(800, 1)) == None
    assert engine.solve_extenders_count(mk_domicile(1000, 1)) == [0,1]      # typical apartment size
    assert engine.solve_extenders_count(mk_domicile(2500, 2)) == [0,2]
    assert engine.solve_extenders_count(mk_domicile(6000, 2)) == [1,3]
    assert engine.solve_extenders_count(mk_domicile(4000, 3)) == [1,3]


def test_solve_extenders(engine):
    result = engine.solve_extenders(mk_domicile(2500, 2))
    assert result != None
    assert result['model'] == 'AM525'
    assert result['count'] == [0,2]


###################################################################################################

def mk_usage(count_residents, count_video4k, count_video, count_gaming):
    return Properties({
        'features': {
            'VOIP': True,
        },
        'usage': [
            {'type': 'RESIDENTS', 'count': count_residents},
            {'type': 'VIDEO4K', 'count': count_video4k},
            {'type': 'VIDEO', 'count': count_video},
            {'type': 'GAMING', 'count': count_gaming},
        ]
    })

def test_calculate_speed_requirements(engine):
    assert engine.calculate_speed_requirements(mk_usage( 0,  0, 0,  0)) == 0
    assert engine.calculate_speed_requirements(mk_usage( 1,  0, 0,  0)) == 3
    assert engine.calculate_speed_requirements(mk_usage(10,  0, 0,  0)) == 30
    assert engine.calculate_speed_requirements(mk_usage(10,  1, 0,  0)) == 55
    assert engine.calculate_speed_requirements(mk_usage(10, 10, 0,  0)) == 280
    assert engine.calculate_speed_requirements(mk_usage(10, 10, 1,  0)) == 290
    assert engine.calculate_speed_requirements(mk_usage(10, 10, 10, 0)) == 380
    assert engine.calculate_speed_requirements(mk_usage(10, 10, 10, 1)) == 386
    assert engine.calculate_speed_requirements(mk_usage(10, 10, 10, 10)) == 440

def test_solve_services_internet(engine):
    result = engine.solve_services_internet(mk_usage(2, 0, 1, 0))
    assert result['name'] == 'Internet 20'
    result = engine.solve_services_internet(mk_usage(4, 0, 2, 1))
    assert result['name'] == 'Internet 100'
    result = engine.solve_services_internet(mk_usage(8, 1, 4, 4))
    assert result['name'] == 'Internet 500'

def test_solve_services_addons(engine):
    result = engine.solve_services_addons(Properties({'features' : {'VOIP' : False}}))
    assert result == []
    result = engine.solve_services_addons(Properties({'features' : {'VOIP' : True}}))
    assert list(map(lambda x: x['name'], result)) == ['Voice']

def test_solve_services(engine):
    result = engine.solve_services(mk_usage(2, 0, 1, 0))
    #my_pprint(result)
    assert result != None
    assert list(map(lambda x: x['name'], result)) == ['Internet 20', 'Voice']



###################################################################################################


def test_solve(engine):
    input = {
        'domicile' : {
            'area' : 1600,
            'floors' : 1,
        },
        'features' : {
            'WAN' : 'DSL',
            'VOIP' : True,
            'MoCA' : False
        },
        'usage' : [
            {'type': 'RESIDENTS', 'count': 4},
            {'type': 'VIDEO4K', 'count': 0},
            {'type': 'VIDEO', 'count': 2},
            {'type': 'GAMING', 'count': 0},
        ],
    }
    result = engine.solve(input)
    #my_pprint(result)
    assert result['hardware']['gateway']['model'] == 'NVG448'
    assert result['hardware']['extenders']['model'] == 'AM525'
    assert result['hardware']['extenders']['count'] == [0,1]
    assert list(map(lambda x: x['name'], result['services'])) == ['Internet 100', 'Voice']



