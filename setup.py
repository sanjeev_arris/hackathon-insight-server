from distutils.core import setup
from setuptools import find_packages

setup(name='eco-insight',
      version='1.0',
      description='Eco Insight Server',
      author='Sanjeev Mishra',
      author_email='smishra@arris.com',
      url='http://insight.arriseco.com/api',
      packages=find_packages(),
      install_requires=['flask-restplus==0.9.2', 'Flask-SQLAlchemy==2.1', 'requests', 'cachetools', 'PyYAML==5.1'],
      )


