insight_rest_apis
=================

This repository contains code for a Insight RESTful API based Flask-RESTPlus.

    virtualenv insight_venv -p python3
    source insight_venv/bin/activate
    python setup.py install
    python server/app.py

Point your browser to localhost:5000/api

Status of the api can be found by calling

http://localhost:5000/api/status/

Flow:
/api/domicile -> returns the domicile information for a given address
/api/recommend -> recommends the package for domicile and usage

## Dependencies

Regenerate the _requirements.txt_ file with the _pipreqs_ utility.

    https://pypi.org/project/pipreqs/

## Unit Tests

The unit tests require the _pytest_ module.

To run unit tests from the commandline, use something like:

    PYTHONPATH=/home/crosenthal/Workspace/hackathon-insight-server pytest