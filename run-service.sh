#!/bin/sh

: ${PYTHON:=python3}
: ${LOG:=service.log}

( set -x ; nohup $PYTHON server/app.py >$LOG 2>&1 ) &

sleep 1
cat >&2 <<_EOT_
Service running ... output directed to: $LOG
*** Monitoring log ... CTRL/C to terminate ***

_EOT_
tail -f $LOG

