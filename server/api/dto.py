import enum

from flask_restplus import fields
from server.api.rest import api


class WanType(enum.Enum):
    CABLE = 'CABLE',
    DSL = 'DSL',
    ETHERNET = 'ETHERNET'


class UsageType(enum.Enum):
    RESIDENTS = 'RESIDENTS',
    VIDEO4K = 'VIDEO4K',
    VIDEO = 'VIDEO',
    GAMING = 'GAMING'


address = api.model('Address', {
    'line_1': fields.String(required=False),
    'line_2': fields.String(required=False),
    'city': fields.String(required=False),
    'state': fields.String(required=False),
    'zip': fields.String(required=False),
})

domicile = api.model('Domicile', {
    'area': fields.Integer(required=True),
    'floors': fields.Integer(required=True),
    'rooms': fields.Integer(required=False),
})

features = api.model('Features', {
    'WAN': fields.String(required=True, description='WAN Type', enum=WanType._member_names_, default=WanType.CABLE.value),
    'VOIP': fields.Boolean(required=True),
    'MoCA': fields.Boolean(required=False),
})

usage = api.model('Usage', {
    'type': fields.String(required=True, description='Primary usage', enum=UsageType._member_names_, default=UsageType.RESIDENTS.value),
    'count': fields.Integer(required=True, description='Number of simultaneous users of the service', default=1),
})

gateway = api.model('Gateway', {
    'model': fields.String(required=False),
    'description': fields.String(required=False),
    'url': fields.String(required=False),
})

extenders = api.model('Extenders', {
    'count': fields.List(fields.Integer(required=True)),
    'description': fields.String(required=False),
    'model': fields.String(required=True),
    'url': fields.String(required=True),
})

service = api.model('Service', {
    'name': fields.String(required=False),
    'description': fields.String(required=False),
})

hardware = api.model('Hardware', {
    'gateway': fields.Nested(gateway),
    'extenders': fields.Nested(extenders),
})

package = api.model('Package', {
    'hardware': fields.Nested(hardware),
    'services': fields.List(fields.Nested(service)),
})

domicile_request = api.model('InitRequest', {
    'address': fields.Nested(address, required=True)
})

reco_request = api.inherit('RecoRequest', domicile_request, {
    'domicile': fields.Nested(domicile),
    'features': fields.Nested(features),
    'usage': fields.List(fields.Nested(usage))
})

procure_request = api.inherit('ProcureRequest', reco_request, {
    'recommendations': fields.Nested(package, required=True)
})

pagination = api.model('A page of results', {
    'page': fields.Integer(description='Number of this page of results'),
    'pages': fields.Integer(description='Total number of pages of results'),
    'per_page': fields.Integer(description='Number of items per page of results'),
    'total': fields.Integer(description='Total number of results'),
})

page_of_procurements = api.inherit('Page of procurements', pagination, {
    'procurements': fields.List(fields.Nested(procure_request))
})
