import json
import logging

from flask_restplus import Resource, fields
from flask import request
from server.api.rest import api
from server.api.dto import procure_request
from server.api.repository import create_subscriber

log = logging.getLogger(__name__)

ns = api.namespace('procure', description='Place order')


@ns.route('/')
class Procure(Resource):
    @api.response(200, 'Placed order')
    @api.expect(procure_request)
    def post(self):
        data = request.json
        log.info('Procurement on the way: {}'.format(json.dumps(data)))
        sub = create_subscriber(data)
        return sub.id, 201

