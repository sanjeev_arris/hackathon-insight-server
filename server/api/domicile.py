import json
import logging

from flask_restplus import Resource, fields
from flask import request
from server.api.rest import api
from server.api.dto import domicile_request


from server.housing_profile.housing_profile import basic_profile

log = logging.getLogger(__name__)

ns = api.namespace('domicile', description='Fetch domicile information')


@ns.route('/')
class Domicile(Resource):
    @api.response(200, 'Fetched profile')
    @api.expect(domicile_request)
    def post(self):
        data = request.json
        log.info('request: {}'.format(json.dumps(data)))

        address = data.get('address')

        if address.get('line_2'):
            address_1 = '{},{}'.format(address['line_1'], address['line_2'])
        else:
            address_1 = address['line_1']

        address_2 = '{}, {}, {}'.format(address['city'], address['state'], address['zip'])

        domicile = basic_profile(address_1, address_2)

        data.update(domicile)

        return data

