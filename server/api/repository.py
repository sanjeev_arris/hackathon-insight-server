from server.database import db
from server.database.models import Subscriber
import json


def create_subscriber(data):
    address = json.dumps(data.get('address'))
    domicile = json.dumps(data.get('domicile'))
    features = json.dumps(data.get('features'))
    usage = json.dumps(data.get('usage'))
    package = json.dumps(data.get('recommendations'))
    status = 'Out for delivery'
    subscriber = Subscriber(address,domicile, features, usage, package, status)
    db.session.add(subscriber)
    db.session.commit()
    return subscriber


def delete_subscriber(sub_id):
    subscriber = Subscriber.query.filter(Subscriber.id == sub_id).one()
    db.session.delete(subscriber)
    db.session.commit()
