import json
from flask_restplus import fields
from server.api.rest import api


class Jsonifier(fields.Raw):
    def format(self, value):
        return json.loads(value)


subscriber = api.model('Subscriber', {
    'id': fields.Integer(readOnly=True, description='The unique identifier of a subscriber'),
    'address': Jsonifier(attribute='address'),
    'domicile': Jsonifier(attribute='domicile'),
    'features': Jsonifier(attribute='features'),
    'usage': Jsonifier(attribute='usage'),
    'package': Jsonifier(attribute='package'),
    'status': fields.String(required=False, description='package status'),
    'created': fields.DateTime,
})

pagination = api.model('A page of results', {
    'page': fields.Integer(description='Number of this page of results'),
    'pages': fields.Integer(description='Total number of pages of results'),
    'per_page': fields.Integer(description='Number of items per page of results'),
    'total': fields.Integer(description='Total number of results'),
})

page_of_subscribers = api.inherit('Page of subscribers', pagination, {
    'items': fields.List(fields.Nested(subscriber))
})
