import logging
from flask import request
from flask_restplus import Resource
from server.api.rest import api
from server.database.models import Subscriber
from server.api.repository import create_subscriber, delete_subscriber
from server.api.parsers import pagination_arguments
from server.api.serializers import subscriber, page_of_subscribers

log = logging.getLogger(__name__)
print(log)
ns = api.namespace('subscribers', description='Operations to register subscribers')


@ns.route('/')
class SubscriberCollection(Resource):
    @api.expect(pagination_arguments, validate=True)
    @api.marshal_with(page_of_subscribers)
    def get(self):
        """
        Returns a page of subscribers:
        """
        args = pagination_arguments.parse_args(request)
        page = args.get('page', 1)
        per_page = args.get('per_page', 10)
        subscribers_query = Subscriber.query
        subscribers_page = subscribers_query.paginate(page, per_page, error_out=False)
        log.debug("fetched the page")
        return subscribers_page


@ns.route('/<int:id>')
@api.response(404, 'Subscriber not found')
class SubscriberItem(Resource):
    @api.marshal_with(subscriber)
    def get(self, id):
        """
        Returns a subscriber:
        """
        log.debug("get for id: {}".format(id))
        sub = Subscriber.query.filter(Subscriber.id == id).one()
        log.debug("result: {}".format(sub))
        return sub

    @api.response(204, 'Subscriber successfully deleted')
    def delete(self, id):
        delete_subscriber(id)
        return None, 204
