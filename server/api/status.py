from flask_restplus import Resource
from server.api.rest import api

ns = api.namespace('status', description='Operations for health check')


@ns.route('/')
class Status(Resource):
    def get(self):
        """
        Returns status
        """
        return 200
