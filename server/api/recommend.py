import json
import logging

from flask_restplus import Resource, fields
from flask import request
from server.api.rest import api
from server.api.dto import reco_request, procure_request


log = logging.getLogger(__name__)

ns = api.namespace('recommend', description='Fetch recommendations')


@ns.route('/')
class Recommend(Resource):
    @api.response(200, 'Fetched recommendation')
    @api.expect(reco_request)
    def post(self):
        data = request.json
        log.info('request: {}'.format(json.dumps(data)))

        result = api.insightEngine.solve(data)

        log.info("result: {}".format(result))

        data['recommendations'] = result

        return data

