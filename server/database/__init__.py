from flask_sqlalchemy import SQLAlchemy

db = SQLAlchemy()


def reset_database():
    from server.database.models import Subscriber
    db.drop_all()
    db.create_all()
