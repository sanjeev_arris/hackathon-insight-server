from server.database import db
from datetime import datetime


class Subscriber(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    address = db.Column(db.String(256))
    domicile = db.Column(db.String(256))
    features = db.Column(db.String(256))
    usage = db.Column(db.String(256))
    package = db.Column(db.String(2048))
    status = db.Column(db.String(16))
    created = db.Column(db.DateTime)

    def __init__(self, address, domicile, features, usage, package, status, created=None):
        self.address = address
        self.domicile = domicile
        self.features = features
        self.usage = usage
        self.package = package
        self.status = status
        if created is None:
            created = datetime.utcnow()
        self.created = created

    def __repr__(self):
        return '<Subscriber {}>'.format(self.address)
