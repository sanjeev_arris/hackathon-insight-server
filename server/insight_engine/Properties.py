
class Properties:
    """Key value store indexed by a property name (dot-separated components)"""

    def __init__(self, data):
        self.data = data

    def getData(self):
        return self.data

    def __getitem__(self, path):
        return self._getRecursive(None, path.split('.'), self.data)

    @classmethod
    def get(cls, path, data):
        return cls(data)[path]

    def _getRecursive(self, path, pendingComponents, data):
        key = pendingComponents.pop(0)
        if path == None:
            path = key
        else:
            path = path + "." + key
        if key not in data:
            raise Exception("Path not found in data: " + path)
        data1 = data[key]
        if len(pendingComponents) == 0:
            return data1
        else:
            return self._getRecursive(path, pendingComponents, data1)

