import math
import yaml
from server.insight_engine.Properties import *

class InsightEngine:
    """The ECO HomeAssure Insight Engine core"""

    def __init__(self, defsFile):
        """
        Initialize the Insight Engine service.

        :param defsFile:
            Path to a YAML file that defines all the parameters for the server.
        """

        with open(defsFile) as f:
            defs = yaml.safe_load(f)
        self.props = Properties(defs)


    def getProperties(self):
        """
        Returns the properties for the service, as defined when the service was initialized.

        :return:
            A tree-structured dict() of the properties.
        """

        return self.props.getData()


    def solve(self, user_input):
        """
        Provide a recommended set of equipment and services that best match the user's needs.

        :param user_input:
            A dict() with the properties that describe the user's needs.

        :return:
            A dict() with the recommended solution.
        """

        # convert the dict() of user_input properties to a keyed Properties() table
        user_props = Properties(user_input)

        # build out the solution
        result = {
            'hardware': {
                'gateway' : self.solve_gateway(user_props),
                'extenders' : self.solve_extenders(user_props),
            },
            'services' : self.solve_services(user_props)
        }

        return result


###################################################################################################

    def solve_gateway(self, user_props):
        """
        Provide a solution for recommended gateway.

        :param user_props:
            A Properties() object with the properties that describe the user's need.
        :return:
            A dict() containing the gateway recommendation.
        """

        wan = user_props['features.WAN']     # The WAN type, such as "DSL"
        moca = user_props['features.MoCA']   # True if MoCA feature is wanted
        voip = user_props['features.VOIP']   # True if VOIP feature is wanted

        for gw in self.props['devices.gateways']:

            # Skip this candidate if it doesn't do what we need.
            if wan not in gw['WAN']:
                continue
            if moca and 'MoCA' not in gw['features']:
                continue
            if voip and 'VOIP' not in gw['features']:
                continue

            # Pick the first gateway that matches all the criteria.
            return {
                'model' : gw['model'],
                'description' : gw['description'],
                'url' : gw['url'],
            }

        return {
            'model' : 'ERROR',
            'description' : 'Sorry, no devices match your selection criteria.',
        }



###################################################################################################

    def solve_extenders(self, user_props):
        """
        Provide a solution for recommended WiFi extender, including count.

        :param user_props:
            A Properties() object with the properties that describe the user's need.
        :return:
            A dict() containing the gateway recommendation.
            Will be None if no extenders are recommended.
        """

        extender_count = self.solve_extenders_count(user_props)
        if extender_count == None:
            return None

        # We're just supporting a single extender type at this time,
        # so just pick the first one listed.
        extender = self.props['devices.extenders'][0]

        return {
            'model' : extender['model'],
            'description' : extender['description'],
            'url' : extender['url'],
            'count' : extender_count,
        }


    def solve_extenders_count(self, user_props):
        """
        Calculate the number of WiFi extenders that should be used to provide full house coverage.

        The algorithm is documented here:
        https://arrisworkassure.atlassian.net/wiki/spaces/ECO/pages/772146317/ECO+Home+Assure+Insights+Engine+--+Core+Design#ECOHomeAssureInsightsEngine--CoreDesign-ExtenderCountModel

        :param user_props:
            A Properties() object with the properties that describe the user's need.
        :return:
            A pair [min,max] that represents a range of number of extenders recommended.
            A good strategy would be for a user to start with the min value, but be aware
            as many as max could be required for full home coverage.
            Returns None if extenders are not recommended for this house (typically because it's small).
        """

        wifi_reach = self.props['config.extender_model.wifi_reach']
        floor_ratio = self.props['config.extender_model.floor_ratio']
        floor_attenuation = self.props['config.extender_model.floor_attenuation']
        safety_factor = self.props['config.extender_model.safety_factor']
        min_adjustment = self.props['config.extender_model.min_adjustment']

        domicile_area = user_props['domicile.area']
        domicile_floors = user_props['domicile.floors']

        # Estimate diagonal of box containing the house, based on square footage.
        diagonal  = math.sqrt( (1+(floor_ratio**2)) * (domicile_area/floor_ratio) )

        # Estimate number of extenders needed to provide full coverage though this house.
        floor_factor = (1-floor_attenuation)**(domicile_floors-1)
        raw_num_extenders = diagonal / ( wifi_reach * floor_factor )
        num_extenders_max = math.floor( raw_num_extenders + safety_factor )

        if num_extenders_max == 0:
            return None

        num_extenders_min = max(0, math.floor(raw_num_extenders - min_adjustment))

        return [num_extenders_min, num_extenders_max]


###################################################################################################

    def solve_services(self, user_props):
        """
        Provide a solution for recommended services: an Internet service package plus any add-on services.

        :param user_props:
            A Properties() object with the properties that describe the user's need.
        :return:
            A dict() containing the services recommendation.
        """

        # First get the recommended Internet service for this user
        services = [ self.solve_services_internet(user_props) ]

        # Merge in any add-on services the user wants
        services += self.solve_services_addons(user_props)

        return services


    def solve_services_internet(self, user_props):
        """
        Recommend an Internet service package for this user, based on their usage profile.

        :param user_props:
            A Properties() object with the properties that describe the user's need.
        :return:
            A dict() containing the recommended Internet service package.
        """

        required_speed = self.calculate_speed_requirements(user_props)
        for service in self.props['services.internet']:
            if service['speed'] >= required_speed:
                return {
                    'name' : service['name'],
                    'description' : service['description'],
                    'url' : service['url']
                }

        return {
            'name' : 'ERROR',
            'description' : 'Sorry, no service offering match your selection criteria.',
        }


    def calculate_speed_requirements(self, user_props):
        """
        Calculate the speed recommended for this user.

        :param user_props:
            A Properties() object with the properties that describe the user's need.
        :return:
            An int value with the speed recommendation in Mbps.
        """

        total = 0
        for usage in user_props['usage']:
            type = usage['type']
            count = usage['count']
            mbps = self.props['config.activity_usage.' + type]
            total += (count * mbps)
        return total


    def solve_services_addons(self, user_props):
        """
        Recommend any add-on services recommended for this user, based on their requested features.

        :param user_props:
            A Properties() object with the properties that describe the user's need.
        :return:
            A list() containing dict() object, which with a recommended add-on service.
            The list will be empty if no add-on services are recommended.
        """

        services = []
        for addon in self.props['services.addons']:
            feature = addon['feature']
            if user_props['features.' + feature] == True:
                services.append({
                    'name' : addon['name'],
                    'description' : addon['description'],
                    'url' : addon['url']
                })
        return services
