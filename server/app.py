import os
import logging.config
from flask import Flask, Blueprint
from server.api.rest import api
from server import settings
# noinspection PyPackageRequirements
from server.api.subscriber.endpoints.subscribers import ns as subscriber_namespace
from server.api.status import ns as status_namespace
from server.api.domicile import ns as domicile_namespace
from server.api.recommend import ns as reco_namespace
from server.api.procure import ns as procure_namespace

from server.database import db

from server.insight_engine import InsightEngine


app = Flask(settings.title)
logging_conf_path = os.path.normpath(os.path.join(os.path.dirname(__file__), '../logging.conf'))
print(logging_conf_path, __name__)
logging.config.fileConfig(logging_conf_path)
log = logging.getLogger(__name__)

engine_seed = os.path.abspath(os.path.join(os.path.dirname(__file__), 'engine-defs.yml'))

log.info("Insight engine configuration file: {}".format(engine_seed))

insightEngine = InsightEngine.InsightEngine(engine_seed)
api.insightEngine = insightEngine


def configure_app(flask_app):
    flask_app.config['SWAGGER_UI_DOC_EXPANSION'] = settings.RESTPLUS_SWAGGER_UI_DOC_EXPANSION
    flask_app.config['RESTPLUS_VALIDATE'] = settings.RESTPLUS_VALIDATE
    flask_app.config['RESTPLUS_MASK_SWAGGER'] = settings.RESTPLUS_MASK_SWAGGER
    flask_app.config['ERROR_404_HELP'] = settings.RESTPLUS_ERROR_404_HELP
    flask_app.config['SQLALCHEMY_DATABASE_URI'] = settings.SQLALCHEMY_DATABASE_URI
    flask_app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = settings.SQLALCHEMY_TRACK_MODIFICATIONS


def initialize_app(flask_app):
    configure_app(flask_app)
    blueprint = Blueprint('api', __name__, url_prefix='/api')
    api.init_app(blueprint)
    api.add_namespace(subscriber_namespace)
    api.add_namespace(status_namespace)
    api.add_namespace(domicile_namespace)
    api.add_namespace(reco_namespace)
    api.add_namespace(procure_namespace)
    flask_app.register_blueprint(blueprint)

    db.init_app(flask_app)
    with app.app_context():
        if settings.DROP_DB:
            db.drop_all()
        db.create_all()


def run():
    initialize_app(app)
    app.run(host='0.0.0.0', debug=settings.FLASK_DEBUG)


if __name__ == '__main__':
    run()
