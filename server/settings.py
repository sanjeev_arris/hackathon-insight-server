# Flask settings
version = '1.0'
title = 'Eco Insight'
description = "REST APIs for Eco Insight"
#FLASK_SERVER_NAME = '0.0.0.0:8888'
#FLASK_SERVER_NAME = ''
FLASK_DEBUG = True  # Do not use debug mode in production

# Flask-Restplus settings
RESTPLUS_SWAGGER_UI_DOC_EXPANSION = 'list'
RESTPLUS_VALIDATE = True
RESTPLUS_MASK_SWAGGER = False
RESTPLUS_ERROR_404_HELP = False

# SQLAlchemy settings
SQLALCHEMY_DATABASE_URI = 'sqlite:///insight.db'
SQLALCHEMY_TRACK_MODIFICATIONS = False
DROP_DB = False
