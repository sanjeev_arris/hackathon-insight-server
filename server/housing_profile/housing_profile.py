import logging
import json

from typing import Dict, Any, Union

import requests
from cachetools import cached, TTLCache
from cachetools.keys import hashkey

cache = TTLCache(100, ttl=24 * 60 * 60)
log = logging.getLogger(__name__)


@cached(cache, key=lambda address1, address2: hashkey(address1.replace(" ", "") + address2.replace(" ", "")))
def basic_profile(address1, address2):
    global response
    log.debug('calling basic profile for address1: %s address2: %s', address1, address2)

    headers = {
        'accept': "application/json",
        'apikey': "0a6c183178445168b56c62bb83c6ed93"  # todo: read it from properties file?
        # 'apikey': "b2cf3ce076dc426c399fb32a243ef0b4"
        # 'apikey': "9bab7e38777671d5960340a02df80ca3"
    }

    # api-endpoint
    url = "https://search.onboard-apis.com/propertyapi/v1.0.0/property/detail"  # todo: read it from properties file?

    # defining a params dict for the parameters to be sent to the API
    params = {'address1': address1, 'address2': address2}

    try:
        # sending get request and saving the response as response object
        response = requests.get(url=url, headers=headers, params=params)
        response.raise_for_status()
    except requests.exceptions.RequestException as e:
        log.error('There was a problem getting a response from profile, returning dummy value ', e)
        return dummy_value()

    # extracting data in json format
    data = response.json()
    # print(json.dumps(data, indent=4))
    log.debug('result for %s, %s is %s ', address1, address2, data)

    # status_code = data.get("status").get("code")

    # if status_code != 0:
    #     log.exception('Returning dummy value. Return status code is %s instead of 0. No information found for address '
    #                   '%s %s ', status_code, address1, address2)
    #     return dummy_value()

    ret_val: Dict[str, Union[int, Any]] = {}

    properties = data.get("property")
    properties_returned = len(properties)
    log.debug("number of properties returned: ", properties_returned)
    if properties_returned != 1:
        log.warning('Got more than one property returned for the address. Going to pick first property in the list.')

    try:
        building = properties[0].get("building")

        living_size = building.get("size").get("livingsize")
        gross_size = building.get("size").get("grosssize")
        rooms = building.get("rooms").get("beds")
        levels = building.get("summary").get("levels")
    except Exception as e:
        log.error('Could not extract building properties ', e)
        return dummy_value()
    log.debug('levels information before guestimate: %s', levels)

    # some of the data returned levels as 0. since levels is required for engine calc, guestimating it here
    stories = levels if levels > 0 else (2 if rooms >= 3 else 1)
    log.debug('levels information after guestimate: %s', stories)

    ret_val['area'] = living_size
    ret_val['gross_size'] = gross_size
    ret_val['rooms'] = rooms
    ret_val['floors'] = stories

    domicile = {'domicile': ret_val}
    log.info('returning information: %s', domicile)
    return domicile


def dummy_value():
    dummy_val = {'area': 0, 'gross_size': 0, 'rooms': 0, 'floors': 0}
    return {'domicile': dummy_val}
